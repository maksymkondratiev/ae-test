// @flow
import * as React from 'react'
import {
  View,
  ScrollView,
  Image,
  Text
} from 'react-native'

import styles from './styles'
import DetailsFooter from './components/DetailsFooter'

type Props = {
  imageUrl: string,
  isLoading: boolean,
  shareCallback: Function,
  applyFilterCallback: Function,
  pictureDetails: Object,
}

// TODO: it would be great to see here loader, pinch to zoom here and pan

class DetailView extends React.PureComponent<Props> {
  render () {
    const { imageUrl, isLoading, shareCallback, applyFilterCallback, pictureDetails } = this.props
    return (
      <View style={styles.container}>
        {
          isLoading ? (<View style={styles.loaderContainer}>
                        <Text style={{color: 'white'}}>Loading...</Text>
                      </View>) : 
          (<View style={styles.container}>
            <ScrollView 
              contentContainerStyle={styles.imageContainer}
              minimumZoomScale={1}
              maximumZoomScale={4}
            >
            <Image
              resizeMode="contain"
              source={{uri: imageUrl}}
              style={styles.imageStyle} />
            </ScrollView>
            <DetailsFooter
              pictureDetails={pictureDetails}
              shareCallback={shareCallback}
              applyFilterCallback={applyFilterCallback}
            />
          </View>
          )
       }
      </View>
    )
  }
}

export default DetailView
