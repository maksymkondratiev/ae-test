// @flow
const API_KEY = '23567b218376f79d9415' // other valid API keys: '760b5fb497225856222a', '0e2a751704a65685eefc'
const API_ENDPOINT = 'http://195.39.233.28:8035'
let accessToken;

export async function getAccessToken(params) {
  try {
    const response = await fetch(`${API_ENDPOINT}/auth`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "apiKey": API_KEY
      })});
    const {token} = await response.json();
    accessToken = token;
  } catch (error) {
    console.log(error);
  }
}

export async function getPictures (page: number = 1): Array<Object> {
  // http://195.39.233.28:8035/images?page=xxx
  try {
    const response = await fetch(`${API_ENDPOINT}/images?page=${page}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }});
    const {pictures} = await response.json();
    return pictures;
  } catch (error) {
    console.log(error);
  }

}

export async function getPictureDetails (id: number): Object {
  // http://195.39.233.28:8035/images/id
  try {
    const response = await fetch(`${API_ENDPOINT}/images/${id}`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${accessToken}`
    }});
    const details = await response.json();
    return details;
  } catch (error) {
    console.log(error)
  }
  
}
