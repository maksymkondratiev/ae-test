import {PICTURE_DETAILS_FETCH_REQUESTED, PICTURE_DETAILS_FETCH_SUCCESS} from './actions';

const initialState = {
  hiResPictures: [],
  isLoading: false,
}

export default function (state: any = initialState, action: Object) {
  const payload = action.payload
  // TODO: implement reducer
  switch (action.type) {
    case PICTURE_DETAILS_FETCH_REQUESTED:
      return {...state, isLoading: true};
    case PICTURE_DETAILS_FETCH_SUCCESS:
      const details = action.payload;
      return {...state, hiResPictures: [...state.hiResPictures, details], isLoading: false};
    default:
      return state;
  }
}
