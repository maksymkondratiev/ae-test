// @flow

import { getPictureDetails } from '../../services/API'
import { FETCH_FAILED } from '../HomeContainer/actions'
import type { ActionWithPayload, ActionWithoutPayload } from '../../types/actions'

export const PICTURE_DETAILS_FETCH_REQUESTED = 'PICTURE_DETAILS_FETCH_REQUESTED'
export const PICTURE_DETAILS_FETCH_SUCCESS = 'PICTURE_DETAILS_FETCH_SUCCESS'

export function pictureIsLoading (): ActionWithoutPayload {
  return {
    type: PICTURE_DETAILS_FETCH_REQUESTED,
  }
}

export function fetchPictureSuccess (details): ActionWithPayload {
  return {
    // TODO: implement me
    type: PICTURE_DETAILS_FETCH_SUCCESS,
    payload: details
  }
}

export function fetchPictureFailed (errorMessage: string): ActionWithPayload {
  return {
    // TODO: implement me
    type: FETCH_FAILED,
    payload: {errorMessage}
  }
}

export function fetchPictureDetails (imageId: number) {
  return async dispatch => {
    // TODO: implement me
    try {
      dispatch(pictureIsLoading());
      const details = await getPictureDetails(imageId);
      dispatch(fetchPictureSuccess(details));
    } catch (error) {
      dispatch(fetchPictureFailed(error));
    }
  }
}
