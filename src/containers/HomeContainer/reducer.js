import {PICTURES_FETCH_REQUESTED, PICTURES_FETCH_SUCCESS, FETCH_FAILED} from './actions';

// @flow
const initialState = {
  pictures: [],
  isLoading: true,
  page: 1,
  errorMessage: '',
}

export default function (state: any = initialState, action: Object) {
  const payload = action.payload
  // TODO: implement reducer
  switch (action.type) {
    case PICTURES_FETCH_REQUESTED:
      return {...state, ...{isLoading: true}};
    case PICTURES_FETCH_SUCCESS:
      const {page, pictures} = action.payload;
      if (page === 1) {
        return {...state, page, pictures, isLoading: false};
      } else {
        return {...state, page, pictures: [...state.pictures, ...pictures], isLoading: false};
      }
    case FETCH_FAILED:
      return {...state, ...action.payload, isLoading: false};
    default: 
      return state;
  }
}
